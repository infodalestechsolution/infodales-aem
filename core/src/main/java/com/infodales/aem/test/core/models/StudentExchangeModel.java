package com.infodales.aem.test.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;
import java.util.List;

@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class StudentExchangeModel {

    @Inject
    List<StudentExchangeList> studentExchangeTile;

    public List<StudentExchangeList> getStudentExchangeTile() {
        return studentExchangeTile;
    }


}
