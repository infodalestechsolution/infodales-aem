package com.infodales.aem.test.core.models;

import java.util.List;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

@Model(adaptables = {Resource.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class VisitsTileModel {
    @Inject
    List<VisitsTile> visitsTile;

    public List<VisitsTile> getVisitsTile() {
        return this.visitsTile;
    }
}
