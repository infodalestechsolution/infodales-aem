package com.infodales.aem.test.core.service;

public interface ApiConfigIDMediaService {
    public String getProtocol();
    public String getHostname();
    public String getRegistrationApi();
    public String getLoginApi();
    public String getTokenValidationApi();
    public String getUserProfileApi();
    public String getForgotPasswordApi();
    public String getProfileApi();
}
