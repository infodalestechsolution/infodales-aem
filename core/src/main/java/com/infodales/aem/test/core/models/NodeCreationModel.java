package com.infodales.aem.test.core.models;

import com.day.cq.commons.jcr.JcrConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class NodeCreationModel {

    String path = "/apps/infodales-aem/components/content";
    @Inject
    ResourceResolver resourceResolver;
    @PostConstruct
    public void init() throws RepositoryException {
        if(path!=null){
            Resource resource = resourceResolver.getResource(path);
            Node demoNode = resource.adaptTo(Node.class);
            if(demoNode != null){
                Node child = demoNode.addNode("testBhai", JcrConstants.NT_UNSTRUCTURED);
                child.setProperty("name","InfodalesBhai");
                child.getSession().save();

            }
        }
    }
}
