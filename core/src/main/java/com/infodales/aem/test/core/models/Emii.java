package com.infodales.aem.test.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;

@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Emii {
    @Inject
    private String emi;
    @Inject
    private String emi2;
    @Inject
    private String way;

    public String getEmi() {
        return emi;
    }

    public String getEmi2() {
        return emi2;
    }

    public String getWay() {
        return way;
    }
}
