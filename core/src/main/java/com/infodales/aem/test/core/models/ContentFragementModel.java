package com.infodales.aem.test.core.models;

import com.adobe.cq.dam.cfm.ContentFragment;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ContentFragementModel {

    @Inject
    List<String> cfLink;

    List<ContentFragementPojo> cflist;

    public List<ContentFragementPojo> getCflist() {
        return cflist;
    }

    @Inject
    ResourceResolver resourceResolver;

    @PostConstruct
    public void init(){
        if(cfLink!=null){
            cflist = new ArrayList<ContentFragementPojo>();
            for (String links:cfLink) {
                ContentFragementPojo cf = new ContentFragementPojo();
                Resource resource = resourceResolver.getResource(links);
                ContentFragment contentFragment = resource.adaptTo(ContentFragment.class);
                cf.setId((contentFragment.getElement("id").getContent()));
                cf.setName(contentFragment.getElement("name").getContent());
                cf.setDate(contentFragment.getElement("date").getContent());
                cf.setDescription(contentFragment.getElement("description").getContent());
                cflist.add(cf);
            }
        }

    }
}
