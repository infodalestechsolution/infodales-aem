package com.infodales.aem.test.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class PageHeader {

    @Inject
    private String title;

    public String getTitle() {
        return title;
    }

    public void changed(){
        title+="sahbaz";
    }
    @PostConstruct
    protected void intit(){
        if(title==null){
            title="Please Enter Title";
        }

    }
}
