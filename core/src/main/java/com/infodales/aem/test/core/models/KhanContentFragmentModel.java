package com.infodales.aem.test.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

@Model(adaptables = Resource.class)
public class KhanContentFragmentModel {

    private String test;
    @PostConstruct
    protected void init() {
        test = "Hello World";
    }

    public String getTest() {
        return test;
    }
}