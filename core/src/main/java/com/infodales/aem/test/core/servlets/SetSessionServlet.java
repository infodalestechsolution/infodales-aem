package com.infodales.aem.test.core.servlets;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Component(service = Servlet.class,
        property = {

                "sling.servlet.methods="+ HttpConstants.METHOD_GET,
                "sling.servlet.methods=" + HttpConstants.METHOD_POST,
                "sling.servlet.paths="+"/bin/infodales/setSession"
        })

public class SetSessionServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(final SlingHttpServletRequest req,
                          final SlingHttpServletResponse resp) throws ServletException, IOException
    {
        String value = req.getParameter("uname");
        HttpSession session = req.getSession();
        session.setAttribute("uname",value);
        resp.getWriter().write("login successfull");

    }

}
