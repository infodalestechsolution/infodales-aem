package com.infodales.aem.test.core.servlets;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Component(service = Servlet.class,
        property = {

                "sling.servlet.methods="+ HttpConstants.METHOD_GET,
                "sling.servlet.methods=" + HttpConstants.METHOD_POST,
                "sling.servlet.paths="+"/bin/infodales/profile"
        })

public class GetSessionServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(final SlingHttpServletRequest req,
                         final SlingHttpServletResponse resp) throws ServletException, IOException
    {
        String url ="http://192.168.0.139:8080/login";
        String value="";
          HttpSession session = req.getSession();
        if(session!=null){
            Object sessionObj = session.getAttribute("uname");

            if(sessionObj!=null){
                value = sessionObj.toString();
            }else{
                value="Session is Empty";
            }
        }else{
            value="Session is not create";
        }


          resp.getWriter().write(value);


    }
}
