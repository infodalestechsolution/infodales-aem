package com.infodales.aem.test.core.service;

import com.infodales.aem.test.core.config.ApiConfigIDMedia;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;

@Component(service = ApiConfigIDMediaService.class)
@Designate(ocd = ApiConfigIDMedia.class)
public class ApiConfigIDMediaServiceImpl implements ApiConfigIDMediaService {

    @Reference
    private ApiConfigIDMedia config;
    @Activate
    public void activate(ApiConfigIDMedia config) {
        this.config = config;

    }


    @Override
    public String getProtocol() {
        return config.protocol();
    }

    @Override
    public String getHostname() {
        return config.hostname();
    }

    @Override
    public String getRegistrationApi() {
        return config.registrationApi();
    }

    @Override
    public String getLoginApi() {
        return config.loginApi();
    }

    @Override
    public String getTokenValidationApi() {
        return config.tokenValidationApi();
    }

    @Override
    public String getUserProfileApi() {
        return config.getuserProfileApi();
    }

    @Override
    public String getForgotPasswordApi() {
        return config.forgotPasswordApi();
    }

    @Override
    public String getProfileApi() {
        return config.getuserProfileApi();
    }
}
