package com.infodales.aem.test.core.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name ="api-osgi configuration", description = "it contains api configuration"

)
public @interface ApiConfigIDMedia {
    @AttributeDefinition(name = "Protocol", description = "Enter the protocol", type = AttributeType.STRING)
    String protocol() default "http://";

    @AttributeDefinition(name = "Hostname", description = "Enter the hostname", type = AttributeType.STRING)
    String hostname() default "localhost";

    @AttributeDefinition(name = "Registration-api", description = "Enter the Registration-api", type = AttributeType.STRING)
    String registrationApi() default "/empty";

    @AttributeDefinition(name = "Login-api", description = "Enter the Login-api", type = AttributeType.STRING)
    String loginApi() default "/empty";

    @AttributeDefinition(name = "tokenValidation-api", description = "Enter the tokenValidation-api", type = AttributeType.STRING)
    String tokenValidationApi() default "/empty";
    @AttributeDefinition(name = "getUserProfile-api", description = "Enter the getUserProfile-api", type = AttributeType.STRING)
    String getuserProfileApi() default "/empty";
    @AttributeDefinition(name = "forgotPassword-api", description = "Enter the forgotPassword-api", type = AttributeType.STRING)
    String forgotPasswordApi() default "/empty";
    @AttributeDefinition(name = "profileUpdate-api", description = "Enter the profileUpdate-api", type = AttributeType.STRING)
    String profileUpdateApi() default "/empty";

}

